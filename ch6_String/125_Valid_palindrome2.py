from collections import deque

def isPalindrome(self, s:str) -> bool:
    strs: Deque = deque()

    for char in s:
        if char.isalpah():
            strs.append(char.lower())

    while len(strs) > 1:
        if strs.popleft() != strs.pop():
            return False

    return True