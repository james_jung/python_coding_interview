from collections import defaultdict

def groupAnagrams(self, strs:list[str]) -> list[list[str]]:
    anagrams = defaultdict(list)

    for word in strs:
        anagrams[''.join(sorted(word))].append(word)
    return anagrams.values()
